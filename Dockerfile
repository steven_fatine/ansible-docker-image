
FROM centos
MAINTAINER fatines (momo@gmail.com)
RUN yum install  -y https://centos7.iuscommunity.org/ius-release.rpm
RUN yum install  -y python36u, python36u-pip
RUN easy_install-3.6 pip
RUN pip3 install --upgrade pip
RUN pip3 install ansible==2.7.0rc4
RUN pip3 install pyvmomi
CMD ["/bin/bash"]
